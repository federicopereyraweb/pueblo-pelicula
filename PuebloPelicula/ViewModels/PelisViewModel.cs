﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PuebloPelicula.Models;

namespace PuebloPelicula.ViewModels
{
    public class PelisViewModel
    {
        public IEnumerable<PeliComercial> Comercial { get; set; }
        public IEnumerable<PeliDocumental> Documental { get; set; }
        public IEnumerable<Pelicula> Pelicula { get; set; }



        public IEnumerable<PeliComercial> Lista { get; set; }

        public IList<Funcion> Funciones { get; set; }
    }
}