﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace PuebloPelicula.Models
{
    public class Sala 
    {

        //[DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "Numero")]
        public int SalaID { get; set; }

        public string NombreSala { get; set; }

        public int CineID { get; set; }

        public virtual Cine Cine { get; set; }

        public bool SalaHD { get; set; }

        
       
    }
}

