﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PuebloPelicula.Models
{
    public class Cine
    {
        public int CineID { get; set; }

        [StringLength(50, MinimumLength = 3)]
        public string Nombre { get; set; }
       
        public string Direccion { get; set; }
    
        public virtual ICollection<Sala> Salas { get; set; }
    }
}