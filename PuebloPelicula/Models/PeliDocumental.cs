﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PuebloPelicula.Models
{
    public enum Tema
    {
        Bio,
        Vida_Salvaje,
        Eco,
        Historia,
        Sociales     
    }

    public class PeliDocumental : Pelicula
    {
        public Tema Tema { get; set; }

    }
}