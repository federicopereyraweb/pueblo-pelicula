﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PuebloPelicula.Models
{
    public enum Tipo
    {
        Comercial,
        Documental
    }

    public abstract class Pelicula
    {

        public int ID { get; set; }

        [Required]
        [StringLength(60)]
        [Display(Name = "Titulo")]
        public string Titulo { get; set; }

        [Required]
        [Range(1, 360)]
        public Int32 Duracion { get; set; }

        //[Required]
        public string Foto { get; set; }

        [NotMapped]
        public HttpPostedFileBase ImageFile { get; set; }


        [StringLength(500)]
        public String Sinopsis { get; set; }
        
        public virtual IList<Funcion> Funciones { get; set; }

    }
}
