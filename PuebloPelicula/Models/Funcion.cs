﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace PuebloPelicula.Models
{
    public class Funcion
    {
        public Funcion()
        {
            Horario = DateTime.Now;
        }


        public int FuncionID { get; set; }
        public int SalaID { get; set; }
        public virtual Sala Sala { get; set; }

        public int PeliculaID { get; set; }
        public virtual Pelicula Pelicula { get; set; }

        [Required]
        [Display(Name = "Horario")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:M/d/yyyy h\\:mm tt}")]
        public DateTime Horario { get; set; }







    }
}