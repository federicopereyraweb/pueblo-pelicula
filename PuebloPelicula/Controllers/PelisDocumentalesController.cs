﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PuebloPelicula.DAL;
using PuebloPelicula.Models;

namespace PuebloPelicula.Controllers
{
    public class PelisDocumentalesController : Controller
    {
        private PuebloPeliContext db = new PuebloPeliContext();

        // GET: PelisDocumentales
        public ActionResult Index()
        {
            return View(db.Peliculas.ToList());
        }

        // GET: PelisDocumentales/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PeliDocumental peliDocumental = db.Documentales.Find(id);
            if (peliDocumental == null)
            {
                return HttpNotFound();
            }
            return View(peliDocumental);
        }

        // GET: PelisDocumentales/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PelisDocumentales/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create(PeliDocumental peliDocumental)
        {
            if (ModelState.IsValid)
            {
                peliDocumental.Foto = peliDocumental.ImageFile.FileName;
                peliDocumental.ImageFile.SaveAs(Server.MapPath("~/Imagenes/Peliculas") + "/" + peliDocumental.ImageFile.FileName);

                db.Peliculas.Add(peliDocumental);
                db.SaveChanges();
                return RedirectToAction("Index", "Peliculas");
            }

            return View(peliDocumental);
        }

        // GET: PelisDocumentales/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PeliDocumental peliDocumental = db.Documentales.Find(id);
            if (peliDocumental == null)
            {
                return HttpNotFound();
            }
            return View(peliDocumental);
        }

        // POST: PelisDocumentales/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Titulo,Duracion,Foto,Sinopsis,Tema")] PeliDocumental peliDocumental)
        {
            if (ModelState.IsValid)
            {
                db.Entry(peliDocumental).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(peliDocumental);
        }

        // GET: PelisDocumentales/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PeliDocumental peliDocumental = db.Documentales.Find(id);
            if (peliDocumental == null)
            {
                return HttpNotFound();
            }
            return View(peliDocumental);
        }

        // POST: PelisDocumentales/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PeliDocumental peliDocumental = db.Documentales.Find(id);
            if (peliDocumental.Funciones.Count() == 0)
            {
                db.Peliculas.Remove(peliDocumental);
                db.SaveChanges();
                return RedirectToAction("Peliculas", "Index");
            }
            ViewBag.Mensaje = "No se puede borrar la película porque está en exhibición";
            return View("Error");

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
