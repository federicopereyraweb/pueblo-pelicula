﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PuebloPelicula.DAL;
using PuebloPelicula.Models;
using PuebloPelicula.ViewModels;

namespace PuebloPelicula.Controllers
{
    public class PeliculasController : Controller
    {
        private PuebloPeliContext db = new PuebloPeliContext();

        // GET: Peliculas
        public ActionResult Index(String Busqueda, int? CineElegido)
        {
          
            var query = from p in db.Peliculas select p;

            var peli_comercial = db.Peliculas.OfType<PeliComercial>().ToList();
            var peli_documental = db.Peliculas.OfType<PeliDocumental>().ToList();
            var pelicula = db.Peliculas.OrderBy(a => a.Titulo).ToList();
          
            var cines = db.Cines.ToList();
            ViewBag.CineElegido = new SelectList(cines, "CineID", "Nombre", CineElegido);
            int CineID = CineElegido.GetValueOrDefault();

            if (Busqueda != null)
            {
                pelicula = pelicula.Where(p => p.Titulo.ToLower().Contains(Busqueda.ToLower()) || p.Sinopsis.ToLower().Contains(Busqueda.ToLower())).ToList();

                peli_comercial = peli_comercial.Where(c => c.Actores.ToLower().Contains(Busqueda.ToLower())).ToList();
                pelicula.AddRange(peli_comercial.ToList());

                peli_documental = peli_documental.Where(d => d.Tema.ToString().ToLower() == Busqueda.ToLower()).ToList();
                pelicula.AddRange(peli_documental.ToList());

            }

            if (CineElegido != null)
            {



                IQueryable<Funcion> funciones = db.Funciones

                    .Where(c => /*!CineElegido.HasValue ||*/ c.Sala.CineID == CineID)
                    .OrderBy(d => d.Sala.CineID)
                    .Include(d => d.Sala.Cine);

                var peliCine = new List<Pelicula>();

                foreach (var f in funciones)
                {
                    peliCine.Add(f.Pelicula);
                }


                pelicula = peliCine.ToList();

            }


            PelisViewModel peliculas = new PelisViewModel()
            {
                Comercial = peli_comercial,
                Documental = peli_documental,
                Pelicula = pelicula,
       
            };

            return View(peliculas);



        }
        //public ActionResult Index()
        //{
        //    return View(db.Peliculas.ToList());
        //}

        // GET: Peliculas/Details/5
        public ActionResult Details(int? id)
        { 
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var comerciales = db.Peliculas.OfType<PeliComercial>();
            var documentales = db.Peliculas.OfType<PeliDocumental>();

            foreach (var pel in comerciales)
            {
                if (pel.ID == id)
                {
                    return RedirectToAction("Details", "PelisComerciales", new { id});
                }
            }
            foreach (var pel in documentales)
            {
                if (pel.ID == id)
                {
                    return RedirectToAction("Details", "PelisDocumentales", new { id });
                }
            }
            ViewBag.MensajeError = "La película no existe";
            return View("Error");
        }

 

        // GET: Peliculas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Peliculas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Titulo,Duracion,Foto,Sinopsis")] Pelicula pelicula)
        {
            if (ModelState.IsValid)
            {
                db.Peliculas.Add(pelicula);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(pelicula);
        }

        // GET: Peliculas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pelicula pelicula = db.Peliculas.Find(id);
            if (pelicula == null)
            {
                return HttpNotFound();
            }
            return View(pelicula);
        }

        // POST: Peliculas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Titulo,Duracion,Foto,Sinopsis")] Pelicula pelicula)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pelicula).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(pelicula);
        }

        // GET: Peliculas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pelicula pelicula = db.Peliculas.Find(id);
            if (pelicula == null)
            {
                return HttpNotFound();
            }
            return View(pelicula);
        }

        // POST: Peliculas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Pelicula pelicula = db.Peliculas.Find(id);
            db.Peliculas.Remove(pelicula);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
