﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PuebloPelicula.DAL;
using PuebloPelicula.Models;

namespace PuebloPelicula.Controllers
{
    public class DocumentalesController : Controller
    {
        private PuebloPeliContext db = new PuebloPeliContext();

        // GET: Documentales
        public ActionResult Index()
        {
            return View(db.Documentales.ToList());
        }

        // GET: Documentales/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PeliDocumental peliDocumental = db.Documentales.Find(id);
            if (peliDocumental == null)
            {
                return HttpNotFound();
            }
            return View(peliDocumental);
        }

        // GET: Documentales/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Documentales/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Titulo,Sinopsis,Duracion,Tema")] PeliDocumental peliDocumental)
        {
            if (ModelState.IsValid)
            {
                db.Peliculas.Add(peliDocumental);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(peliDocumental);
        }

        // GET: Documentales/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PeliDocumental peliDocumental = db.Documentales.Find(id);
            if (peliDocumental == null)
            {
                return HttpNotFound();
            }
            return View(peliDocumental);
        }

        // POST: Documentales/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Titulo,Sinopsis,Duracion,Tema")] PeliDocumental peliDocumental)
        {
            if (ModelState.IsValid)
            {
                db.Entry(peliDocumental).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(peliDocumental);
        }

        // GET: Documentales/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PeliDocumental peliDocumental = db.Documentales.Find(id);
            if (peliDocumental == null)
            {
                return HttpNotFound();
            }
            return View(peliDocumental);
        }

        // POST: Documentales/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PeliDocumental peliDocumental = db.Documentales.Find(id);
            db.Peliculas.Remove(peliDocumental);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
