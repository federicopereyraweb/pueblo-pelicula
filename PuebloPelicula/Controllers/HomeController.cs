﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PuebloPelicula.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Peliculas");
        }



        [Authorize]
        public ActionResult Admin()
        {
            ViewBag.Message = "Administración de cartelera";

            return View();
        }
    }
}