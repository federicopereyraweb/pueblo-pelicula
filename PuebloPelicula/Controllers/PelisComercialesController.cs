﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PuebloPelicula.DAL;
using PuebloPelicula.Models;

namespace PuebloPelicula.Controllers
{
    public class PelisComercialesController : Controller
    {
        private PuebloPeliContext db = new PuebloPeliContext();

        // GET: PelisComerciales
        public ActionResult Index()
        {
            return View(db.Peliculas.ToList());
        }

        // GET: PelisComerciales/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PeliComercial peliComercial = db.Comerciales.Find(id);
            if (peliComercial == null)
            {
                return HttpNotFound();
            }
            return View(peliComercial);
        }

        // GET: PelisComerciales/Create
        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        // POST: PelisComerciales/Create
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PeliComercial peliComercial)
        {
            if (ModelState.IsValid)
            {
                peliComercial.Foto = peliComercial.ImageFile.FileName;
                peliComercial.ImageFile.SaveAs(Server.MapPath("~/Imagenes/Peliculas") + "/" + peliComercial.ImageFile.FileName);

                db.Peliculas.Add(peliComercial);
                db.SaveChanges();
                return RedirectToAction("Index", "Peliculas");
            }

            //return View(peliComercial);
            //if (ModelState.IsValid)
            //{
            //    db.Peliculas.Add(peliComercial);
            //    db.SaveChanges();
            //    return RedirectToAction("Index");
            //}

            return View(peliComercial);
        }

        // GET: PelisComerciales/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PeliComercial peliComercial = db.Comerciales.Find(id);
            if (peliComercial == null)
            {
                return HttpNotFound();
            }
            return View(peliComercial);
        }

        // POST: PelisComerciales/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Titulo,Duracion,Foto,Sinopsis,Actores")] PeliComercial peliComercial)
        {
            if (ModelState.IsValid)
            {
                db.Entry(peliComercial).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(peliComercial);
        }

        // GET: PelisComerciales/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PeliComercial peliComercial = db.Comerciales.Find(id);
            if (peliComercial == null)
            {
                return HttpNotFound();
            }
            return View(peliComercial);
        }

        // POST: PelisComerciales/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PeliComercial peliComercial = db.Comerciales.Find(id);
            if(peliComercial.Funciones.Count() == 0)
            {
                db.Peliculas.Remove(peliComercial);
                db.SaveChanges();
                return RedirectToAction("Index", "Peliculas");
            }
            ViewBag.Mensaje = "No se puede borrar la película porque está en exhibición";
            return View("Error");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
