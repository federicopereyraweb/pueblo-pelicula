﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PuebloPelicula.DAL;
using PuebloPelicula.Models;

namespace PuebloPelicula.Controllers
{
    public class SalasController : Controller
    {
        private PuebloPeliContext db = new PuebloPeliContext();

        // GET: Salas
        public ActionResult Index()
        {
            var salas = db.Salas.Include(s => s.Cine);
            return View(salas.ToList());
        }

        // GET: Salas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sala sala = db.Salas.Find(id);
            if (sala == null)
            {
                return HttpNotFound();
            }
            return View(sala);
        }

        // GET: Salas/Create
        public ActionResult Create()
        {
            ViewBag.CineID = new SelectList(db.Cines, "CineID", "Nombre");
            return View();
        }

        // POST: Salas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SalaID,NombreSala,CineID,SalaHD")] Sala sala)
        {
            if (ModelState.IsValid)
            {
                db.Salas.Add(sala);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CineID = new SelectList(db.Cines, "CineID", "Nombre", sala.CineID);
            return View(sala);
        }

        // GET: Salas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sala sala = db.Salas.Find(id);
            if (sala == null)
            {
                return HttpNotFound();
            }
            ViewBag.CineID = new SelectList(db.Cines, "CineID", "Nombre", sala.CineID);
            return View(sala);
        }

        // POST: Salas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SalaID,NombreSala,CineID,SalaHD")] Sala sala)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sala).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CineID = new SelectList(db.Cines, "CineID", "Nombre", sala.CineID);
            return View(sala);
        }

        // GET: Salas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sala sala = db.Salas.Find(id);
            if (sala == null)
            {
                return HttpNotFound();
            }
            return View(sala);
        }

        // POST: Salas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Sala sala = db.Salas.Find(id);
            db.Salas.Remove(sala);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        public ActionResult BorrarTodas(int? id)
        {


            //Sala sala = db.Salas.Find(id);
            ////var funciones = db.Funciones.Include(f => f.Pelicula).Include(f => f.Sala);
            //sala = db.Funciones.Where(f => f.SalaID == sala.SalaID).Include(fu => fu.Pelicula);
            //db.Funciones.RemoveRange(db.Funciones.Where(f => f.SalaID == id));
            //db.SaveChanges();
            Sala sala = db.Salas.Find(id);
            var funciones = db.Funciones.Include(f => f.Pelicula).Include(f => f.Sala).Where(f => f.SalaID == id).ToList();


            db.Funciones.RemoveRange(db.Funciones.Where(f => f.SalaID == id));
            db.SaveChanges();
            return RedirectToAction("Index", "Funciones");


        }

        [HttpPost, ActionName("BorrarTodas")]
        public ActionResult BorrarTodas(int id)
        {


            Sala sala = db.Salas.Find(id);
            var funciones = db.Funciones.Include(f => f.Pelicula).Include(f => f.Sala).Where(f => f.SalaID == id).ToList();
           

            db.Funciones.RemoveRange(db.Funciones.Where(f => f.SalaID == id));
            db.SaveChanges();
            return RedirectToAction("Index", "Funciones");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
