﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PuebloPelicula.DAL;
using PuebloPelicula.Models;
using System.Data.Entity.Infrastructure;

namespace PuebloPelicula.Controllers
{
    public class ComercialesController : Controller
    {
        private PuebloPeliContext db = new PuebloPeliContext();

        // GET: Comerciales
        public ActionResult Index()
        {
            return View(db.Comerciales.ToList());
        }

        // GET: Comerciales/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PeliComercial peliComercial = db.Comerciales.Find(id);
            if (peliComercial == null)
            {
                return HttpNotFound();
            }
            return View(peliComercial);
        }

        // GET: Comerciales/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Comerciales/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Titulo,Sinopsis,Duracion,Actores")] PeliComercial peliComercial)
        {
            if (ModelState.IsValid)
            {
                db.Peliculas.Add(peliComercial);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(peliComercial);
        }

        // GET: Comerciales/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PeliComercial peliComercial = db.Comerciales.Find(id);
            if (peliComercial == null)
            {
                return HttpNotFound();
            }
            return View(peliComercial);
        }

        // POST: Comerciales/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Titulo,Sinopsis,Duracion,Actores")] PeliComercial peliComercial)
        {
            if (ModelState.IsValid)
            {
                db.Entry(peliComercial).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(peliComercial);
        }

        // GET: Comerciales/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PeliComercial peliComercial = db.Comerciales.Find(id);
            if (peliComercial == null)
            {
                return HttpNotFound();
            }
            return View(peliComercial);
        }

        // POST: Comerciales/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PeliComercial peliComercial = db.Comerciales.Find(id);
            db.Peliculas.Remove(peliComercial);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
