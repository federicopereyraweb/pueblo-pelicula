﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PuebloPelicula.DAL;
using PuebloPelicula.Models;

namespace PuebloPelicula.Controllers
{
    public class FuncionesController : Controller
    {
        private PuebloPeliContext db = new PuebloPeliContext();

        // GET: Funciones
        public ActionResult Index()
        {
            var funciones = db.Funciones.Include(f => f.Pelicula).Include(f => f.Sala);
            return View(funciones.ToList());
        }

        // GET: Funciones/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Funcion funcion = db.Funciones.Find(id);
            if (funcion == null)
            {
                return HttpNotFound();
            }
            return View(funcion);
        }

        // GET: Funciones/Create
        [Authorize]
        public ActionResult Create()
        {
            ViewBag.PeliculaID = new SelectList(db.Peliculas, "ID", "Titulo");
            ViewBag.SalaID = new SelectList(db.Salas, "SalaID", "NombreSala");
            return View();
        }

        // POST: Funciones/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create([Bind(Include = "FuncionID,SalaID,PeliculaID,Horario")] Funcion funcion)
        {

            if (db.Peliculas.Find(funcion.PeliculaID).GetType().Name.Contains("PeliDocumental"))
            {
             
                if (db.Salas.Find(funcion.SalaID).SalaHD)
                {
                    ViewBag.Mensaje = "No se puede exhibir un documental en usa sala HD";
                    return View("Error");
                }
            }
           
            var dia = funcion.Horario.Date;
            
            var funcionesDia = db.Funciones.ToList();
            funcionesDia = funcionesDia.Where(f => f.SalaID == funcion.SalaID).ToList();

            var funcionesCargadas = new List<Funcion>();


            foreach (var f in funcionesDia)
            {
                if (f.Horario.Date == dia)
                {
                    funcionesCargadas.Add(f);
                }

                if (f.Horario.Date == dia.AddDays(1) && f.Horario.Hour < 10)
                {
                    funcionesCargadas.Add(f);
                }
            }

            funcionesCargadas.OrderBy(f => f.Horario);


            if (!funcionesCargadas.Any() || (funcionesCargadas.Last().Horario.Hour < 10 && funcionesCargadas.Last().Horario.Date == funcion.Horario.Date) )
            {
                TimeSpan ts = new TimeSpan(10, 0, 0);
                funcion.Horario = funcion.Horario.Date + ts;
            }
            else
            {
                var ultima = funcionesCargadas.Last();
                var nuevoHorario = ultima.Horario.AddMinutes(db.Peliculas.Find(ultima.PeliculaID).Duracion);
                nuevoHorario = nuevoHorario.AddMinutes(15);

                TimeSpan ts = new TimeSpan(2, 0, 0);
                var horarioLimite = dia + ts;
                horarioLimite = horarioLimite.AddDays(1);

                if (nuevoHorario < horarioLimite)
                {
                    funcion.Horario = nuevoHorario;
                }
                else
                {
                    ViewBag.MensajeError = "No hay horarios para el día elegido";
                    return View("Error");
                }


            }



            if (ModelState.IsValid)
            {
                db.Funciones.Add(funcion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PeliculaID = new SelectList(db.Peliculas, "ID", "Titulo", funcion.PeliculaID);
            ViewBag.SalaID = new SelectList(db.Salas, "SalaID", "NombreSala", funcion.SalaID);
            return View(funcion);
        }

        // GET: Funciones/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Funcion funcion = db.Funciones.Find(id);
            if (funcion == null)
            {
                return HttpNotFound();
            }
            ViewBag.PeliculaID = new SelectList(db.Peliculas, "ID", "Titulo", funcion.PeliculaID);
            ViewBag.SalaID = new SelectList(db.Salas, "SalaID", "NombreSala", funcion.SalaID);
            return View(funcion);
        }

        // POST: Funciones/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit([Bind(Include = "FuncionID,SalaID,PeliculaID,Horario")] Funcion funcion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(funcion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PeliculaID = new SelectList(db.Peliculas, "ID", "Titulo", funcion.PeliculaID);
            ViewBag.SalaID = new SelectList(db.Salas, "SalaID", "NombreSala", funcion.SalaID);
            return View(funcion);
        }

        // GET: Funciones/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Funcion funcion = db.Funciones.Find(id);
            if (funcion == null)
            {
                return HttpNotFound();
            }
            return View(funcion);
        }

        // POST: Funciones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult DeleteConfirmed(int id)
        {
            Funcion funcion = db.Funciones.Find(id);
            db.Funciones.Remove(funcion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
