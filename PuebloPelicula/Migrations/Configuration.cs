﻿namespace PuebloPelicula.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Models;

    internal sealed class Configuration : DbMigrationsConfiguration<PuebloPelicula.DAL.PuebloPeliContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }


        protected override void Seed(PuebloPelicula.DAL.PuebloPeliContext context)
        {
            context.Peliculas.AddOrUpdate(p => p.ID,
              new PeliComercial
              {
                  Titulo = "Spiderman",
                  Foto = "spiderman.jpg",
                  Duracion = 120,
                  Actores = "Tom Holland,Samuel L.Jackson,Zendaya Jones, Cobie Smulders ",
                  Sinopsis = "Peter Parker decide pasar unas merecidas vacaciones en Europa junto a MJ, Ned y el resto de sus amigos. Sin embargo, Peter debe volver a ponerse el traje de Spider-Man cuando Nick Fury le encomienda una nueva misión: frenar el ataque de unas criaturas que están causando el caos en el continente"
              },
               new PeliComercial
               {
                   Titulo = "Dumbo",
                   Foto = "dumbo.jpg",
                   Duracion = 109,
                   Actores = "Danny DeVito, Colin Farrell, Nico Parker, Finley Hobbins",
                   Sinopsis = "El dueño de un circo en aprietos contrata a un hombre y sus dos hijos para cuidar de un elefante recién nacido que puede volar, que pronto se convierte en la atracción principal que revitaliza al circo."
               },
               new PeliComercial
               {
                   Titulo = "Toc Toc",
                   Foto = "toc-toc.jpg",
                   Duracion = 96,
                   Actores = "Rossy de Palma, Paco León, Alexandra Jiménez, Nuria Herrero, Adrián Lastra, Oscar Martínez",
                   Sinopsis = "Toc Toc es una película española de comedia dirigida por Vicente Villanueva en el año 2017, producida por LAZONA y ATRESMedia y distribuida por Warner Bros Pictures. Es una adaptación de la obra teatral homónima del comediógrafo francés Laurent Baffie"
               },
               new PeliComercial
                {
                    Titulo = "Toy Story 4",
                    Foto = "toy.jpg",
                    Duracion = 100,
                    Actores = "Tom Hanks (voz de Woody), Tony Hale (voz de Forky), Annie Potts (voz de Bo Peep)",
                    Sinopsis = "Toy Story 4 es una película estadounidense de comedia animada de 2019, la cuarta y última entrega de la saga de animación Toy Story y la secuela de Toy Story 3 (2010). Es producida por Pixar Animation Studios, y fue lanzada por Walt Disney Pictures"
                },
                
                new PeliComercial
                {
                    Titulo = "Annabelle",
                    Foto = "anna.jpg",
                    Duracion = 80,
                    Actores = "Annabelle Wallis5, Ward Horton, Alfred Woodard",
                    Sinopsis = "Annabelle es una película estadounidense de terror dirigida por John R. Leonetti y escrita por Gary Dauberman. Se trata de una precuela y una secuela de la película The Conjuring."
                },
                new PeliComercial
                {
                    Titulo = "El Padrino",
                    Foto = "elpadrino1.jpg",
                    Duracion = 125,
                    Actores = "Marlon Brando, Al Pacino, James Caan",
                    Sinopsis = "El Padrino (título original en inglés: The Godfather1​) es una película estadounidense de 1972 dirigida por Francis Ford Coppola. El filme fue producido por Albert S. Ruddy, de la compañía Paramount Pictures. Está basada en la novela homónima (que a su vez está basada en la familia real de los Mortillaro de Sicilia), de Mario Puzo, quien adaptó el guion junto a Coppola y Robert Towne, este último sin ser acreditado"
                },
                new PeliDocumental
                {
                    Titulo = "Chernobyl",
                    Foto = "cherno.jpg",
                    Duracion = 45,

                    Tema = Tema.Historia,

                    Sinopsis = "Chernobyl (en español: Chernóbil) es un drama histórico creado por Craig Mazin. La serie es una coproducción entre los canales HBO de Estados Unidos y Sky de Reino Unido, se estrenó en Estados Unidos el 6 de mayo de 2019 y en Reino Unido el 7 de mayo de 2019. La emisión de la miniserie de cinco capítulos finalizó el 3 de junio de 2019."
                },
                new PeliDocumental
                {
                    Titulo = "Bohemian Rhapsody",
                    Foto = "Bohemian.jpg",
                    Duracion = 60,

                    Tema = Tema.Bio,

                    Sinopsis = "Bohemian Rhapsody es una película biográfica británica-estadounidense de 2018 sobre el cantante británico Freddie Mercury y el grupo de rock Queen.​ Fue dirigida por Bryan Singer, aunque Dexter Fletcher asumió el cargo de director las últimas semanas de rodaje tras el despido de Singer.El guion fue escrito por Anthony McCarten y fue producida por Graham King y Jim Beach, antiguo mánager de Queen."
                },
                new PeliDocumental
                {
                    Titulo = "Free Solo",
                    Foto = "free-solo.jpg",
                    Duracion = 72,

                    Tema = Tema.Bio,

                    Sinopsis = "Documental que narra la hazaña de Alex Honnold al escalar sin ningún tipo de ayuda la formación rocosa vertical El Capitán, ubicada dentro del Parque Nacional Yosemite (Mariposa County, Estados Unidos), de unos 900 metros de altura. Fue el primer escalador en lograrlo"
                },
                new PeliDocumental
                {
                    Titulo = "AMUNDSEN, EL PRIMERO EN EL POLO SUR",
                    Foto = "amundsen.jpg",
                    Duracion = 75,
                    
                    Tema = Tema.Historia, 
                    
                    Sinopsis = "Escenas filmadas hace más de 100 años en las que se puede ver al noruego Roald Amundsen en los días previos al comienzo de la aventura que le llevaría hasta el polo Sur, coronando una de las gestas más famosas de todos los tiempos."
                });

            context.Cines.AddOrUpdate(c => c.CineID,
               new Cine
               {
                   CineID = 1,
                   Nombre = "Caballito",
                   Direccion = "Acoyte y Rivadavia"
               },

                new Cine
                {
                    CineID = 2,
                    Nombre = "Flores",
                    Direccion = "Rivadavia y Carabobo"

                });

                context.Salas.AddOrUpdate(s => s.SalaID,
                new Sala
                {
                    SalaID = 1,
                    CineID = 1,
                    NombreSala = "Caballito1",
                    SalaHD = true
                },

                new Sala
                {
                    SalaID = 2,
                    CineID = 1,
                    NombreSala = "Caballito2",
                    SalaHD = false

                },

                new Sala
                {
                    SalaID = 3,
                    CineID = 2,
                    NombreSala = "Flores1",
                    SalaHD = true
                },

                new Sala
                {
                    SalaID = 4,
                    CineID = 2,
                    NombreSala = "Flores2",
                    SalaHD = false

                });

            context.Funciones.AddOrUpdate(f => f.FuncionID,
                new Funcion
                {
                    FuncionID = 1,
                    SalaID = 1,
                    PeliculaID = 1,
                    Horario = new DateTime(2019, 7, 5, 10, 0, 0)
                   

                });

       
        }
    }
}
