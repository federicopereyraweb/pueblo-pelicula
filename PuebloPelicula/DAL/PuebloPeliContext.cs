﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PuebloPelicula.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;


namespace PuebloPelicula.DAL
{
    public class PuebloPeliContext : DbContext
    {
        public PuebloPeliContext()
       : base("DefaultConnection") { }

        public DbSet<Cine> Cines { get; set; }
        public DbSet<Sala> Salas { get; set; }
        public DbSet<Funcion> Funciones { get; set; }
        public DbSet<Pelicula> Peliculas { get; set; }
        public DbSet<PeliComercial> Comerciales { get; set; }
        public DbSet<PeliDocumental> Documentales { get; set; }


    

    }
}