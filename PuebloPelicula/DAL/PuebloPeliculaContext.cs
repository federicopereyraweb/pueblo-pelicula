﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PuebloPelicula.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace PuebloPelicula.DAL
{
    public class PuebloPeliculaContext : DbContext
    {
        
        
            public DbSet<Sala> Salas { get; set; }
            public DbSet<Cine> Cines { get; set; }
            public DbSet<Funcion> Funciones { get; set; }
            public DbSet<Pelicula> Peliculas { get; set; }
            public DbSet<PeliComercial> Comerciales { get; set; }
            public DbSet<PeliDocumental> Documentales{ get; set; }

        public System.Data.Entity.DbSet<PuebloPelicula.Models.Funcion> Funcions { get; set; }


        /* 
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
         {
             modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

             modelBuilder.Entity<Course>()
                 .HasMany(c => c.Instructors).WithMany(i => i.Courses)
                 .Map(t => t.MapLeftKey("CourseID")
                     .MapRightKey("InstructorID")
                     .ToTable("CourseInstructor"));

             modelBuilder.Entity<Department>().MapToStoredProcedures();
         }
         */

    }
}